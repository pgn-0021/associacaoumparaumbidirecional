package br.edu.principal;

import br.edu.oodb.Pessoa;
import br.edu.oodb.Residencia;

public class Principal {

	public static void main(String[] args) {

		//Adicionando duas pessoas e duas residencias;
		Residencia res1 = new Residencia();
		res1.setNumero("201-A");
		
		Residencia res2 = new Residencia();
		res2.setNumero("201-B");
		
		Pessoa bernardo = new Pessoa();
		bernardo.setNome("Bernardo");
		
		Pessoa aline = new Pessoa();
		aline.setNome("Aline");
		
		//Associando uma residencia para aline e para bernardo;
		bernardo.associaResidencia(res1);
		aline.associaResidencia(res2);
		
		//Imprimindo as residencias de aline e bernardo
		System.out.println("Nº da casa de bernardo: " +bernardo.getResidencia().getNumero());
		System.out.println("Nº da case de Aline: "+aline.getResidencia().getNumero());
		System.out.println();
		
		//Trocando as residencias
		bernardo.associaResidencia(res2);

		System.out.println("Nº da casa de bernardo: " +bernardo.getResidencia().getNumero());
				
		aline.associaResidencia(res1);

		System.out.println("Nº da case de Aline: "+aline.getResidencia().getNumero());
		System.out.println();
		
		//Ou então
		
		System.out.println("A residencia "+res2.getNumero()+" pertence a: "+res2.getPessoa().getNome());
		System.out.println("A residencia "+res1.getNumero()+" pertence a: "+res1.getPessoa().getNome());
		
	}

}
