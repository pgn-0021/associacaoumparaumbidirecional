package br.edu.oodb;

public class Pessoa {

	private String nome;

	/**
	 * attributo representando associacao
	 */
	private Residencia residencia;
	
	public void removeResidencia(){
		this.residencia = null;
	}
	
	public void associaResidenciaAux(Residencia residencia){
		this.residencia = residencia;
	}
	
	public void associaResidencia(Residencia residencia){
		if (residencia.getPessoa() != null){
			residencia.getPessoa().removeResidencia();
		}
		this.associaResidenciaAux(residencia);
		residencia.associaPessoaAux(this);
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
		
	public Residencia getResidencia(){
		return this.residencia;
	}
	
}
