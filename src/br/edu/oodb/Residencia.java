package br.edu.oodb;

public class Residencia {

	private String numero;
	/**
	 * attributo representando associacao
	 */
    private Pessoa pessoa;
    
    public void removePessoa(){
    	this.pessoa = null;
    }
    
    public void associaPessoaAux(Pessoa pessoa){
    	this.pessoa = pessoa;
    }
    
    public void associaPessoa(Pessoa pessoa){
    	if (pessoa.getResidencia() != null) {
    		pessoa.getResidencia().removePessoa();
    	}
    	this.associaPessoaAux(pessoa);
    	pessoa.associaResidenciaAux(this);    	
    }
	
	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Pessoa getPessoa(){
		return pessoa;
	}
	
}
